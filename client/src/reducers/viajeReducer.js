import { GET_VIAJES, ADD_VIAJE, DELETE_VIAJE, VIAJES_LOADING, GET_VIAJE, FILTER_VIAJES, SEARCH_VIAJES_BY_CIUDAD, 
    RESET_SEARCH, 
    SEARCH_CIUDADES_BY_VIAJE} from '../actions/types';

const initialState = {
    viajes: [],
    viajesFiltered: [],
    paquete: [],
    ciudades_viaje: [],
    loading: false,
    searchtxt: ''
};

export default function(state = initialState, action) {
    switch(action.type) {
        case GET_VIAJES:
            return {
                ...state,
                viajes: action.payload[0],
                loading: false
            };
        case DELETE_VIAJE:
            return {
                ...state,
                viajes: state.viajes.filter(viaje => viaje._id !== action.payload)
            }
        case ADD_VIAJE:
            return {
                ...state,
                viajes: [action.payload, ...state.viajes] // adds new viaje
            }
        case VIAJES_LOADING:
            return {
                ...state,
                loading: true
            }
        case GET_VIAJE:
            return {
                ...state,
                paquete: action.payload[0][0],
                loading: false
            }
        case FILTER_VIAJES:
            return{
                ...state,
                viajesFiltered: action.payload[0],
                searchtxt: action.payload[1]
            }
        case SEARCH_VIAJES_BY_CIUDAD:
            return{
                ...state,
                viajesFiltered: action.payload[0],
                loading: false
            }
        case SEARCH_CIUDADES_BY_VIAJE:
            return{
                ...state,
                ciudades_viaje: action.payload[0][0],
                loading: false
            }
        case RESET_SEARCH:
            return{
                ...state,
                viajesFiltered: initialState.viajesFiltered,
            }
        default:
                return state;
    }
}