import { combineReducers } from 'redux';
import viajeReducer from './viajeReducer';
import ciudadReducer from './ciudadReducer';
// import other reducer ex: ciudadReducer, eventoReducer
export default combineReducers({
    viaje: viajeReducer,
    //evento: eventoReducer
    ciudad: ciudadReducer
});