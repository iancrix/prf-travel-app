import { GET_CIUDADES, CIUDADES_LOADING } from '../actions/types';

const initialState= {
    ciudades: [],
    loading: false
};

export default function(state = initialState, action) {
    switch(action.type) {
        case GET_CIUDADES:
            return {
                ...state,
                ciudades: action.payload[0],
                loading: false
            };
        case CIUDADES_LOADING:
            return {
                ...state,
                loading: true
            };
        default:
            return state;
    }
}