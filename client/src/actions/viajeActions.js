import axios from 'axios';
import { GET_VIAJES, ADD_VIAJE, DELETE_VIAJE, VIAJES_LOADING, GET_VIAJE, FILTER_VIAJES, SEARCH_VIAJES_BY_CIUDAD,
RESET_SEARCH, SEARCH_CIUDADES_BY_VIAJE} from './types';

/* export const getViajes = () => dispatch => {
    dispatch(setViajesLoading());
    axios
        .get('/api/viajes') //proxy in package.json react
        .then(res => 
            dispatch({
                type: GET_VIAJES,
                payload: res.data
            })
        );
}; */

export const getViajes = () => dispatch => {
    dispatch(setViajesLoading());
    axios
        .get('/api/viajes/DAOMySQL') //proxy in package.json react
        .then(res => 
            dispatch({
                type: GET_VIAJES,
                payload: res.data
            })
        );
};

export const addViaje = (viaje) => dispatch => {
    axios
        .post('/api/viajes', viaje)
        .then(res => 
            dispatch({
                type: ADD_VIAJE,
                payload: res.data // the new viaje
            })
        );
};

export const deleteViaje = (id) => dispatch => {
    axios.delete(`/api/viajes/${id}`) // delete from server
        .then(res =>dispatch({
            type: DELETE_VIAJE,
            payload: id //dispatch to the reducer
        })
    );
};

export const setViajesLoading = () => {
    return {
        type: VIAJES_LOADING
    }
};

export const getViaje = (id) => dispatch => {
    dispatch(setViajesLoading());
    axios
        .get(`/api/viajes/DAOMySQL/${id}`) //proxy in package.json react
        .then(res => 
            dispatch({
                type: GET_VIAJE,
                payload: res.data
            })
        );
};

export const filterViajes = (filteredViajes, searchtxt) => {
    return {
        type: FILTER_VIAJES,
        payload: [filteredViajes, searchtxt]
    }
};

export const searchViajesByCiudad = (id) => dispatch => {
    dispatch(setViajesLoading());
    axios
        .get(`/api/viajes/DAOMySQL2/${id}`) //proxy in package.json react
        .then(res => 
            dispatch({
                type: SEARCH_VIAJES_BY_CIUDAD,
                payload: res.data
            })
        );
};

export const searchCiudadesByViaje = (id) => dispatch => {
    axios
        .get(`/api/viajes/DAOMySQL3/${id}`) //proxy in package.json react
        .then(res => 
            dispatch({
                type: SEARCH_CIUDADES_BY_VIAJE,
                payload: res.data
            })
        );
};

export const resetSearch = () => {
    return {
        type: RESET_SEARCH,
    }
};
