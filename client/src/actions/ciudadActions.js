import axios from 'axios';
import { GET_CIUDADES, CIUDADES_LOADING } from './types';

export const getCiudades = () => dispatch => {
    dispatch(setCiudadesLoading());
    axios
        .get('/api/ciudades/DAOMySQL') //proxy in package.json react
        .then(res => 
            dispatch({
                type: GET_CIUDADES,
                payload: res.data
            })
        );
};

export const setCiudadesLoading = () => {
    return {
        type: CIUDADES_LOADING
    }
};

