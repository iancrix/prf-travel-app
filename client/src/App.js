import React, { Component } from 'react';
import AppNavbar from './components/AppNavbar';
import Main from './components/Main';
import ViajeForm from './components/ViajeForm';
import Checklist from './components/Checklist';
import Checklists from './components/Checklists';
import PaquetesList from './components/PaquetesList';
import EventosList from './components/EventosList';
import Slider from './components/Slider';
import Footer from './components/Footer';
import Newsletter from './components/Newsletter';
import Paquete from './components/Paquete';
import Chatbot from './components/Chatbot';
import BlogEntry from './components/BlogEntry';
import BlogsList from './components/BlogsList';
import NewPost from './components/NewPost';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <Provider store={store}>
          <Chatbot show="0" />
          <AppNavbar />

        <div className="App">
        
          
          <Route exact path='/' render={props => (
            <React.Fragment>
              <Checklist />
              <Checklists />
              <PaquetesList />
            </React.Fragment>
          )} />  

          <Route path='/contact' render={props => (
            <React.Fragment>
              <Newsletter/>
            </React.Fragment>
            
          )} />
          <Route path='/blog/:id' component={BlogEntry} />
          <Route exact path='/pk/:id' render={props => (
            <React.Fragment >
              <Paquete {...props}/>
              <EventosList {...props}/>
            </React.Fragment>
          )} />
            <Route path='/allblogs' render={props => (
              <React.Fragment>
                <BlogsList />
              </React.Fragment>
            )} />
            <Route path='/newPost' render={props => (
              <React.Fragment>
                <NewPost />
              </React.Fragment>
            )} />
            <Footer/>
        </div>
      </Provider>
      </Router>
      
    );
  }
}

export default App;

/*import React, { Component } from 'react';
import AppNavbar from './components/AppNavbar';
import Main from './components/Main';
import ViajeForm from './components/ViajeForm';
import PaquetesList from './components/PaquetesList';
import Slider from './components/Slider';
import Footer from './components/Footer';
import Newsletter from './components/Newsletter';
import Paquete from './components/Paquete';
import Chatbot from './components/Chatbot';
import BlogEntry from './components/BlogEntry';
import BlogsList from './components/BlogsList';
import newPost from './components/newPost';

import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <Provider store={store}>
          <Chatbot show="0" />
          <AppNavbar />


          <div className="App">


            <Route exact path='/' render={props => (
              <React.Fragment>
                <Slider />
                <ViajeForm />
                <PaquetesList />
              </React.Fragment>
            )} />

            <Route path='/contact' render={props => (
              <React.Fragment>
                <Newsletter />

                <Footer />
              </React.Fragment>
            )} />
            <Route path='/blog/:id' component={BlogEntry} />
            <Route path='/pk/:id' component={Paquete} />
            <Route path='/allblogs' render={props => (
              <React.Fragment>
                <BlogsList />
              </React.Fragment>
            )} />
            <Route path='/newPost' render={props => (
              <React.Fragment>
                <newPost />
              </React.Fragment>
            )} />

          </div>
        </Provider>
      </Router>

    );
  }
}

export default App;*/