import React, { Component } from 'react'

import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getViaje, searchCiudadesByViaje } from '../actions/viajeActions';

import 'bootstrap/dist/css/bootstrap.min.css';
import './CSS/Paquete.css';

class Paquete extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ID_Viaje: this.props.match.params.id,
            ciudad_1: null,
            numberOfAdults: 0,
            numberOfKids: 0,
            subtotal: 0
        };
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        
        //console.log('Adults ' + this.state.numberOfAdults);
        //console.log('Kids' + this.state.numberOfKids);
        //console.log('Precio Adult ' + this.props.viaje.ciudades_viaje.precio_adulto);
        //console.log('Precio Kids ' + this.props.viaje.ciudades_viaje.precio_adulto);
        var subtotal = this.props.viaje.paquete.precio_adulto * this.state.numberOfAdults +
        this.props.viaje.paquete.precio_menor * this.state.numberOfKids;

        console.log('Subtotal ' + subtotal);

        this.setState({
          [name]: value,
          subtotal: subtotal
        });
    }


    componentDidMount(){
        this.props.getViaje(this.state.ID_Viaje);
        this.props.searchCiudadesByViaje(this.state.ID_Viaje);
    }

    render() {
        const { paquete, ciudades_viaje } = this.props.viaje;
        console.log(ciudades_viaje.precio_adulto);
        console.log(ciudades_viaje.id_ciudad);
        console.log(paquete.descripcion);
        return (
            <div>
                <form>
                <div className='packet'>
                    <div className='pk-img-1' 
                    src='https://cdn.pixabay.com/photo/2017/12/03/04/20/titlis-2994226_960_720.jpg'
                    alt='lol'
                    >
                    </div>
                    <div className='pk-img-2'></div>
                    <div className='-pk'>
                        <div className='pk-origen'>
                            <div className='pk-origen-1'>
                                <img className='img-default' src='https://2bwrl244x46j3rrorox6yc6b-wpengine.netdna-ssl.com/wp-content/uploads/migration_images/beautiful-bogota/bogota_shutterstock_377863483-759x500.jpg' alt='lol'></img>
                                <div className='pk-origen-1-1'>
                                    <p>Bogotá</p>
                                </div> 
                            </div>
                            <div className='pk-origen-2-1'>
                                <p className='aerolinea'>Aerolínea</p>
                            </div>
                            <div className='pk-origen-2-2'>
                                <p className='aeroitem'>{paquete.id_aerolinea}</p>
                            </div>
                            <div className='pk-origen-3'>
                                <p className='informacion'>Bogotá is Colombia’s sprawling, high-altitude capital. La Candelaria, its cobblestoned center, features colonial-era landmarks like the neoclassical performance hall Teatro Colón and the 17th-century Iglesia de San Francisco. It's also home to popular museums including the Museo Botero, showcasing Fernando Botero's art, and the Museo del Oro, displaying pre-Columbian gold pieces.</p>
                            </div>
                            <div className='pk-origen-4'>
                                <img 
                                src='https://png.pngtree.com/svg/20160630/c01b8dd79d.png'
                                alt='lol'
                                className='img-origen'
                                ></img>
                            </div>
                        </div>
                        <div className='pk-destino'>
                        <div className='pk-origen-1'>
                            <img className='img-default' src={ciudades_viaje.thumbnail} alt='lol'></img>
                                <div className='pk-destino-1-1'>
                                    <p>{ciudades_viaje.nombre}</p>
                                </div> 
                            </div>
                            <div className='pk-destino-2-1'>
                                <p className='aeroitem-des'>{paquete.id_aerolinea}</p>
                            </div>
                            <div className='pk-destino-2-2'>
                                <p className='aerolinea-des'>Aerolínea</p>
                            </div>
                            <div className='pk-origen-3'>
                                <p className='informacion'>{ciudades_viaje.informacion}</p>
                            </div>
                            <div className='pk-origen-4'>
                                <img 
                                src='https://png.pngtree.com/svg/20160630/c01b8dd79d.png'
                                className='img-destino'
                                ></img>
                            </div>
                        </div>
                        <div className='pk-info'>
                            <div className='pk-info-cont'>
                                <div className='pk-info-1'>
                                    <p className='info-nombre'>{paquete.nombre}</p>
                                </div>
                                <div className='pk-info-2'>
                                    <div className='pk-info-2-1'>
                                        <p className='desc-txt'>{paquete.descripcion}</p>
                                    </div>
                                    <div className='pk-info-2-2'>
                                        <div className='pk-info-2-2-txt'>
                                            <h1 className='cupos'>{paquete.cupos_restantes}</h1>
                                            <p className='cupos-txt'>Cupos <br></br>restantes</p>
                                        </div>
                                    </div>
                                </div>
                                <div className='pk-info-titles'>
                                    <div className='pk-info-ida'>
                                        <p className='title-txt'>Ida</p>
                                    </div>
                                    <div className='pk-info-vuelta'>
                                        <p className='title-txt'>Vuelta</p>
                                    </div>
                                </div>
                                <div className='pk-info-3'>
                                    <div className='pk-info-3-1'>

                                    </div>
                                    <div className='pk-info-3-2'>
                                        <img src='https://cdn4.iconfinder.com/data/icons/small-n-flat/24/calendar-512.png'
                                            alt=''
                                            className='img-info-1'
                                        ></img>
                                    </div>
                                    <div className='pk-info-3-3'>
                                        <p>00-00-0000</p>
                                    </div>
                                    <div className='pk-info-3-4'>
                                    
                                    </div>
                                    <div className='pk-info-3-5'>
                                        <img src='https://cdn4.iconfinder.com/data/icons/small-n-flat/24/calendar-512.png'
                                            alt=''
                                            className='img-info-1'
                                        ></img>
                                    </div>
                                    <div className='pk-info-3-6'>
                                        <p>00-00-0000</p>
                                    </div>
                                    <div className='pk-info-3-7'>

                                    </div>
                                </div>
                                <div className='pk-info-4'>
                                    <div className='pk-info-4-1'>
                                        <div className='pk-info-4-1-1'>
                                            <img src='https://blacklabelagency.com/wp-content/uploads/2017/08/money-icon.png'
                                                alt=''
                                                className='img-info-2'
                                            ></img>
                                        </div>
                                        <div className='pk-info-4-1-2'>
                                            <p className='precio-txt'>Precios</p>
                                        </div>
                                       
                                    </div>          
                                    <div className='pk-info-4-2'>
                                        <div className='pk-info-4-2-1'>
                                            <div className='pk-info-4-2-1-1'>
                                                <img src='https://img.icons8.com/color/420/person-male.png'
                                                alt=''
                                                className='img-person'
                                                ></img>
                                            </div>
                                            <div className='pk-info-4-2-1-2'>
                                               <p className='x-txt'>X</p> 
                                            </div>
                                            <div className='pk-info-4-2-1-3'>
                                            <img src='https://i.dlpng.com/static/png/331426_thumb.png'
                                                alt=''
                                                className='img-symbol'
                                                ></img>
                                            </div>
                                            <div className='pk-info-4-2-1-4'>
                                                <p className='price-txt'>{paquete.precio_adulto}</p>
                                            </div>
                                            <div className='pk-info-4-2-1-5'></div>
                                            <div className='pk-info-4-2-1-6'>
                                                <img src='https://newvitruvian.com/images/transparent-avatars-kid-5.png'
                                                alt=''
                                                className='img-kid'
                                                ></img>
                                            </div>
                                            <div className='pk-info-4-2-1-7'>
                                                <p className='x-txt'>X</p> 
                                            </div>
                                            <div className='pk-info-4-2-1-8'>
                                                <img src='https://i.dlpng.com/static/png/331426_thumb.png'
                                                className='img-symbol'
                                                ></img>
                                            </div>
                                            <div className='pk-info-4-2-1-9'>
                                                <p className='price-txt'>{paquete.precio_menor}</p>
                                            </div>
                                            <div className='pk-info-4-2-1-10'></div>
                                        </div>
                                        <div className='pk-info-4-2-2'>

                                        </div>
                                        <div className='pk-info-4-2-3'>
                                            <p className='viajeros-txt'>
                                                Selecciona la cantidad de viajeros:
                                            </p>
                                        </div>
                                        <div className='pk-info-4-2-4'>
                                            <div className='pk-info-4-2-4-1'>
                                                <div className='pk-info-4-2-4-1-1'>
                                                    <div className='adult-icon'>
                                                        <img src='https://img.icons8.com/color/420/person-male.png'
                                                        alt=''
                                                        className='adult'
                                                        ></img>
                                                    </div>
                                                    <div className='text-icon'>
                                                        <p>
                                                         Adultos
                                                        </p>
                                                    </div>
                                                </div>
                                                <div className='pk-info-4-2-4-1-2'>
                                                    <input
                                                      className='input-viajeros'
                                                      name='numberOfAdults'
                                                      type='number'
                                                      value={this.state.numberOfAdults}
                                                      onChange={this.handleInputChange}
                                                    ></input>
                                                </div>
                                            </div>
                                            <div className='pk-info-4-2-4-2'>
                                                <div className='pk-info-4-2-4-2-1'>
                                                    <div className='kid-icon'>
                                                        <img src='https://newvitruvian.com/images/transparent-avatars-kid-5.png'
                                                        alt=''
                                                        className='kid'
                                                        ></img>
                                                    </div>
                                                    <div className='text-icon -color-kid'>
                                                        <p>
                                                         Niños
                                                        </p>
                                                    </div>
                                                </div>
                                                <div className='pk-info-4-2-4-2-2'>
                                                    <input
                                                      className='input-viajeros'
                                                      name='numberOfKids'
                                                      type='number'
                                                      value={this.state.numberOfKids}
                                                      onChange={this.handleInputChange}
                                                    ></input>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className='pk-info-5'>
                                    <div className='pk-info-5-1'>
                                        <p className='subtotal'>Subtotal</p>
                                    </div>
                                    <div className='pk-info-5-2'>
                                        <p className='subtotal-precio'>
                                            {this.state.subtotal}
                                        </p>
                                    </div>
                                    
                                </div>

                                
                            </div>
                        </div>

                    </div>
                </div>
                </form>
            </div>
        )
    }
}

Paquete.propTypes = {
    viaje: PropTypes.object.isRequired,
    getViaje: PropTypes.func.isRequired,
    searchCiudadesByViaje: PropTypes.func.isRequired
  }

const mapStateToProps = (state) => ({ //mapping redux state to component property
    viaje: state.viaje
  });

export default connect(mapStateToProps, { getViaje, searchCiudadesByViaje })(Paquete);
