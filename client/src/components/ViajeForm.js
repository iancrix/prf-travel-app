import React, { Component } from 'react';
import './CSS/ViajeForm.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { connect } from 'react-redux';
import { getViajes, filterViajes, searchViajesByCiudad, resetSearch } from '../actions/viajeActions';
import { getCiudades } from '../actions/ciudadActions';
import PropTypes from 'prop-types';

class ViajeForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ciudad: '',
      fecha_inicio: null,
      fecha_fin: null,
      currentlyDisplayed: this.props.viaje,
      load: false
    };
    this.handleInicio = this.handleInicio.bind(this);
    this.handleFin = this.handleFin.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onInputChange = this. onInputChange.bind(this);
  }
  
  componentDidMount() {
    this.props.getViajes();
    this.props.getCiudades();
  }

  onInputChange(event){

    //let newlyDisplayed = this.props.viaje.viajes.filter(viaje => viaje.nombre.toLowerCase().includes(event.target.value.toLowerCase()));
    var evento = event.target.value;
    let ciudadPicker = this.props.ciudad.ciudades.filter(ciudad => ciudad.nombre.toLowerCase().includes(event.target.value.toLowerCase()));
    if(ciudadPicker.length > 0 && !(event.target.value === ''))
    {
      this.props.searchViajesByCiudad(ciudadPicker[0].id_ciudad);
    }else if(ciudadPicker.length === 0 && !(event.target.value === '')){
      this.props.filterViajes([], event.target.value);
    }else{
      setTimeout((evento) => this.props.filterViajes(this.props.viaje.viajes, evento), 600);
      
    }
    /*
    if(!(event.target.value === ''))
    {   
      let ciudadPicker = this.props.ciudad.ciudades.filter(ciudad => ciudad.nombre.toLowerCase().includes(event.target.value.toLowerCase()));
      if(!(ciudadPicker.length === 0) && this.props.viaje.loading === false)
      {
        this.props.searchViajesByCiudad(ciudadPicker[0].id_ciudad);
      }
    }else{
      setTimeout(() => this.props.resetSearch(), 500);
    }
    */
    this.setState({
      ciudad: event.target.value,
      //currentlyDisplayed: newlyDisplayed,
    })
    
  }

  handleInicio(date) {
    this.setState({
      fecha_inicio: date
    });
  }
  handleFin(date) {
    this.setState({
      fecha_fin: date
    });
  }

  onChange = (e) => this.setState({ [e.target.name]:
    e.target.value });

  

 onSubmit = (e) => {
     e.preventDefault();
     this.props.addTodo(this.state.title);
     this.setState({ ciudad: '' });
 }
   
  render() {
    return (
      <div>
        <div className='search-box'>
            <div className='search-box-outer-container'>
                <div className='search-box-header'>
                    <span className='box-title'>Paquetes turísticos</span>
                </div>

                <form onSubmit={this.onSubmit}>
                <div className='box-row -wrap'>
                    <div className='box-row box-proportion'>
                        <div className='box-origen'>
                            <label className='box-label'>Ciudad</label>
                            <div className='box-input-container -mb4-s'>
                                <div className='box-input'>
                                    <input 
                                        type='text' 
                                        name='origen' 
                                        id='origen' 
                                        placeholder='Ingresa una ciudad' 
                                        autoComplete='disabled'
                                        className='input-tag'
                                        value={this.state.ciudad}
                                        onChange={this.onInputChange}
                                        ></input>
                                        {this.state.textfield}
                                    </div>
                                </div>    
                            </div>
                            
                        </div>

                    <div className='box-row -wrap box-dates-container'>
                        <div className='box-dates-labels'>
                            <label className='box-dates-label box-label'>Fechas</label>
                        </div>
                        <div className='box-dates-row'>
                            <div className='box-dates-ida'>
                                <DatePicker
                                    id='Inicio'
                                    name='Inicio'
                                    placeholderText='Ida'
                                    selected={this.state.fecha_inicio}
                                    onChange={this.handleInicio}
                                    autoComplete='disabled'
                                    className='input-tag'
                                    dateFormat='yyyy-MM-dd'
                                />
                            </div>
                            <div className='box-dates-vuelta'>
                                <DatePicker
                                    id='Fin'
                                    name='Fin'
                                    placeholderText='Vuelta'
                                    selected={this.state.fecha_fin}
                                    onChange={this.handleFin}
                                    autoComplete='disabled'
                                    className='input-tag'
                                    dateFormat='yyyy-MM-dd'
                                />
                            </div>
                        </div>
                    </div>

                    
                </div> 
                </form>
            </div>
       </div>    

      </div>
      
    )
  }
}

ViajeForm.propTypes = {
    getViajes: PropTypes.func.isRequired,
    getCiudades: PropTypes.func.isRequired,
    searchViajesByCiudad: PropTypes.func.isRequired,
    resetSearch: PropTypes.func.isRequired,
    filterViajes: PropTypes.func.isRequired,
    viaje: PropTypes.object.isRequired
  }
  
  const mapStateToProps = (state) => ({ //mapping redux state to component property
    viaje: state.viaje,
    ciudad: state.ciudad
  });
  
  export default connect(mapStateToProps, { getViajes, filterViajes, getCiudades, searchViajesByCiudad, resetSearch })(ViajeForm);
