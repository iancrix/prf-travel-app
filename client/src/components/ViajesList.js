import React, { Component } from 'react';
import { Container, ListGroup, ListGroupItem, Button } from 'reactstrap';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { connect } from 'react-redux';
import { getViajes, deleteViaje } from '../actions/viajeActions';
import PropTypes from 'prop-types';

class ViajesList extends Component {
  componentDidMount() {
      this.props.getViajes();
  }

  onDeleteClick = (id) => {
    this.props.deleteViaje(id);
  }

  render() {
      const { viajes } = this.props.viaje;
    return (
      <Container>
          <ListGroup>
              <TransitionGroup className="viajes-list">
                {viajes.map(({ _id, nombre }) => ( // What to do with each Viaje
                  <CSSTransition key={_id} timeout={500} classNames="fade">
                  <ListGroupItem>
                    <Button
                        className="remove-btn"
                        color="danger"
                        size="sm"
                        onClick={this.onDeleteClick.bind(this, _id)}
                    >&times;</Button>
                    {nombre}
                  </ListGroupItem>
               </CSSTransition> 
                ))}
              </TransitionGroup>
          </ListGroup>
      </Container>
    )
  }
}

ViajesList.propTypes = {
    getViajes: PropTypes.func.isRequired,
    viaje: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({ //mapping redux state to component property
    viaje: state.viaje
});

export default connect(mapStateToProps, { getViajes, deleteViaje })(ViajesList);

