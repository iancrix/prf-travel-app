import React,{ Component } from "react"
import './CSS/newPost.css'
import {Form, FormGroup, Label, Input, Button} from 'reactstrap'
import axios from 'axios'
import httpsProxyAgent from 'https-proxy-agent'


class NewPost extends Component{
  constructor(props){
    super(props)
    this.state = { titulo: "titul", autor: "aut", contenido: "cont", thumbnail: "",}
  }

  changedForm(obj,tipo){
    console.log(obj.target.value);
    switch (tipo) {
      case "titulo":
        this.setState({titulo:obj.target.value})
        break;
      case "autor":
        this.setState({autor: obj.target.value})
        break;
      case "contenido":
        this.setState({ contenido: obj.target.value })
        break;
      case "thumbnail":
        this.setState({ thumbnail: obj.target.value })
        break;
      default:
        throw Error();
    }
  }

  componentDidMount(){
    var agent = new httpsProxyAgent('http://localhost:5000')
    var config = {
      url: '/api/viajes/blog/newpost',
      httpsAgent:agent,
      method:'post',
      data:this.state,
      headers:{'content-Type':'application/json'}
    }
    axios.request(config).then(console.log).catch(console.log)
  }

  submitForm(event){
    
    event.preventDefault();
    console.log(this.state.titulo)
    console.log(this.state.autor)
    console.log(this.state.contenido)
    console.log(this.state.thumbnail)
    console.log("Conectando...")
    var agent = new httpsProxyAgent('http://localhost:5000')
    var config = {
      url: '/api/viajes/blog/newpost',
      httpsAgent:agent,
      method:'post',
      data:this.state,
      headers:{'content-Type':'application/json'}
    }

    axios.request(config)
      .then((res) => {
        window.location = "/allblogs"
      })
      .catch(err => {
        console.log(err)
        this.setState({
          error: true
        })
      })
  }

  render(){
    return (
      <div className="div-total-final">
        <div className="form-div">
        <Form className="form-form" onSubmit={(e)=>{this.submitForm(e)}}>
          <FormGroup>
            <Label for="tituloPost">Titulo</Label>
            <Input id="tituloPost" name="titulo" placeholder="Titulo" onChange={(ev)=>this.changedForm(ev,"titulo")}/>
          </FormGroup>
          <FormGroup>
            <Label for="autorPost">Autor</Label>
            <Input id="autorPost" name="autor" placeholder="Autor" onChange={(ev) => this.changedForm(ev, "autor")}/>
          </FormGroup>
          <FormGroup>
            <Label for="contenidoPost">Contenido</Label>
            <Input type="textarea" id="contenidoPost" name="contenido" rows="10" placeholder="Escribe aquí tus fabulosas historias de viaje!!" onChange={(ev) => this.changedForm(ev, "contenido")} />
          </FormGroup>
          <FormGroup>
            <Label>Thumbnail</Label>
            <Input id="thumbnailPost" name="thumbnail" placeholder="Sube tu imagen a Pinterest (o a otro servicio de alojamiento de imagenes) y pon aquí el link!" onChange={(ev) => this.changedForm(ev, "thumbnail")}/>
          </FormGroup>
          <FormGroup>
            <Button>Submit</Button>
          </FormGroup>
        </Form>
      </div>
      </div>
    )
  }
}

export default NewPost