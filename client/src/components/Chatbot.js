import React, { Component } from 'react'
import './CSS/Chatbot.css';
import axios from 'axios';
import httpsProxyAgent from 'https-proxy-agent'

class Chat extends Component{
  constructor (props){
    super(props);
    this.state={remitente:props.remitente,mensaje:props.mensaje};
  }
  render(){
    return this.state.remitente === "1"
               ? <div><div className="chat-left" dangerouslySetInnerHTML={{ __html: this.state.mensaje}}></div></div>
               : <div><div className="chat-right">{this.state.mensaje}</div></div>
  }
}

class Chatbot extends Component {
  constructor(props){
    super(props);
    this.state={...props,chats:[{remitente:"1",chat:"Buenos días, en que le podemos colaborar?"}]};
    this.chatbotInput = React.createRef();
    this.chatbotContent = React.createRef();
    this.chatbotMax = React.createRef();
    this.chatbotMin = React.createRef();
  }
  componentDidMount(){
    /*setInterval(() => {
      this.chatbotMensaje("galo")
    }, 1000);*/
    //this.tick();
    
  }
  render() {
    //console.log("fdsaf", this.state.chats.map((element) => { return <Chat remitente="1" mensaje={ element }/>}),"arrodsfdlsa","fdsafkj")
    //if(this.state.show === "1"){
      var chatsvisual = this.state.chats.map((element) => { return <Chat remitente={element.remitente} mensaje={element.chat} /> });//((element)=>{return <Chat remitente="1" mensaje={element}/>});
      return (
        <div>
          <div className="chatbot oculto" ref={this.chatbotMax}>
            <div className="chatbot-header" onClick={this.minimizar.bind(this)}>PRF Travel</div>
            <div className="chatbot-content" ref={this.chatbotContent}>{chatsvisual}</div>

            <div className="chatbot-footer"><input className="chatbot-input-field" ref={this.chatbotInput} onKeyPress={this.checkEnter.bind(this)} /></div>
          </div>
          <div className="chatbot-min" ref={this.chatbotMin}>
            <div className="chatbot-header-min" onClick={this.maximizar.bind(this)}>
              <div className="chatbot-header-min1"></div>
              <div className="chatbot-header-min2">Chatbot</div>
            </div>

          </div>
        </div>
      )
      //< button onClick = { this.sendMensaje.bind(this,"0") } > Enviar</button > //Ubicar frente al input si se quiere tener boton de chatbot
    //}
  }

  checkEnter(e){
    //alert(e.key)
    if(e.key==="Enter")this.sendMensaje();
  }

  chatbotMensaje(mensaje){
    console.log(mensaje);
    this.setState((state, props) => ({
      chats: [...state.chats, { remitente: "1", chat: mensaje.data.mensaje }]
    }), () => {
      if (this.chatbotContent.current)this.chatbotContent.current.scrollBy(0, 99999);
    })
  }

  sendMensaje(){
    if(this.chatbotInput.current.value !== ""){
      this.setState((state,props)=>({
        chats: [...state.chats, { remitente: "0", chat:this.chatbotInput.current.value}]
      }), () => {
          console.log(this.chatbotInput.current.value)
          var agent = new httpsProxyAgent('http://localhost:5000')
          var config = {
            url: '/api/viajes/chatbot',
            httpsAgent: agent,
            method: 'get',
            params: {mensaje:this.chatbotInput.current.value},
            headers: { 'content-Type': 'application/json' }
          }
          axios.request(config)
            .then((res) => {
              this.chatbotMensaje(res)
            })
            .catch(err => {
              //console.log(err)
              this.setState({
                error: true
              })
            })

          this.chatbotInput.current.value = "";
          this.chatbotContent.current.scrollBy(0, 99999);
          

          
      });
    }
  }
  
  minimizar(){
    //this.setState({show:"0"});
    this.chatbotMax.current.classList.add('oculto')
    this.chatbotMin.current.classList.remove('oculto')
  }

  maximizar() {
    //this.setState({ show: "1" });
    this.chatbotMin.current.classList.add('oculto')
    this.chatbotMax.current.classList.remove('oculto')
  }

}



export default Chatbot;
