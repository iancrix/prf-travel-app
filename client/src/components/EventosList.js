import React, { Component } from 'react'
import './CSS/EventosList.css';

class EventosList extends Component {
    render() {
        return (
            <div>
                
                <div className='cont-fill'></div>
                <div className='-event'>
                    <div className='eve-1' 
                        src='https://villageatthewoodlandswaterway.com/wp-content/uploads/ThinkstockPhotos-526833490.jpg'
                        alt='lol'
                        >
                    
                        <div className='-eve -title'>
                            <p className='eve-title-txt'>Eventos</p>  
                        </div>
                        <div className='-eve -cont'>
                            <div className='item-titles'>
                                <div className='item-titles-1'>
                                    <p className='txt-eve-title'>Seleccionar</p>
                                </div>
                                <div className='item-titles-2'>
                                    <p className='txt-eve-title'>Evento</p>
                                </div>
                                <div className='item-titles-3'>
                                    <p className='txt-eve-title'>Fecha inicio</p>
                                </div>
                                <div className='item-titles-4'>
                                    <p className='txt-eve-title'>Fecha fin</p>
                                </div>
                                <div className='item-titles-5'>
                                    <p className='txt-eve-title'>Precio</p>
                                </div>
                                <div className='item-titles-6'>
                                    <p className='txt-eve-title'>Ciudad</p>
                                </div>
                            </div>
                            <div className='eve-item'>

                                <div className='item-1 '>
                                    <input 
                                        type="checkbox" 
                                        name="event" 
                                        value="Concierto"
                                        className='-chk'
                                    ></input>
                                </div>
                                <div className='item-2'>
                                    <p className='item-txt'>Nombre</p>
                                </div>
                                <div className='item-3'>
                                    <p className='item-txt'>Inicio</p>
                                </div>
                                <div className='item-4'>
                                    <p className='item-txt'>Fin</p>
                                </div>
                                <div className='item-5'>
                                    <p className='item-txt'>Precio</p>
                                </div>
                                <div className='item-6'>
                                    <p className='item-txt'>Ciudad</p>
                                </div>
                                
                            </div>
                             <div className='eve-item'>

                                <div className='item-1 '>
                                    <input 
                                        type="checkbox" 
                                        name="event" 
                                        value="Concierto"
                                        className='-chk'
                                    ></input>
                                </div>
                                <div className='item-2'>
                                    <p className='item-txt'>Nombre</p>
                                </div>
                                <div className='item-3'>
                                    <p className='item-txt'>Inicio</p>
                                </div>
                                <div className='item-4'>
                                    <p className='item-txt'>Fin</p>
                                </div>
                                <div className='item-5'>
                                    <p className='item-txt'>Precio</p>
                                </div>
                                <div className='item-6'>
                                    <p className='item-txt'>Ciudad</p>
                                </div>
                                
                            </div>
                             <div className='eve-item'>

                                <div className='item-1 '>
                                    <input 
                                        type="checkbox" 
                                        name="event" 
                                        value="Concierto"
                                        className='-chk'
                                    ></input>
                                </div>
                                <div className='item-2'>
                                    <p className='item-txt'>Nombre</p>
                                </div>
                                <div className='item-3'>
                                    <p className='item-txt'>Inicio</p>
                                </div>
                                <div className='item-4'>
                                    <p className='item-txt'>Fin</p>
                                </div>
                                <div className='item-5'>
                                    <p className='item-txt'>Precio</p>
                                </div>
                                <div className='item-6'>
                                    <p className='item-txt'>Ciudad</p>
                                </div>
                                
                            </div>
                            
                            

                           
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default EventosList;
