import React, {Component} from 'react'
import './CSS/ViajeForm.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class Checklist extends Component {
    constructor(props) {
        super(props);
        this.state = {
          textfield : false
        };
        this.handleInputChange = this.handleInputChange.bind(this);
      }

      handleInputChange(e){
        const target = e.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        if(target.name == 'otro' && target.checked == true){
            this.setState({textfield : true});
        } else{
            this.setState({textfield : false});
        }

        if(!(target.name == 'otro'))
        {
            this.setState({
            [name]: value
        });
    }
        
    }

    renderInputField(){
        if(this.state.textfield == true){
            return (<input
            type='text'
            name='hola'
            value='hola'>            
            </input>);
        }
    }



    render() {
        return (
          <div>
            <div className='search-box'>
                <div className='search-box-outer-container'>
                    <div className='search-box-header'>
                        <span className='box-title'>Accidentes Catastróficos</span>
                    </div>
    
                    <form onSubmit={this.onSubmit}>
                    <div className='box-row -wrap'>
                        <div className='box-row box-proportion'>
                            <div className='box-origen'>
                                <label className='box-label'>---o---</label>
                                <div className='box-input-container -mb4-s'>
                                    <div className='box-input'>
                                            <input 
                                            type='checkbox' 
                                            name='incendio' 
                                            value='incendio'
                                            onChange={this.handleInputChange}
                                            /> Incendio <br></br>
                                            <input 
                                            type='checkbox' 
                                            name='inundacion' 
                                            value='inundacion'
                                            onChange={this.handleInputChange}
                                            /> Inundacion <br></br>
                                            <input 
                                            type='checkbox' 
                                            name='sismo' 
                                            value='sismo'
                                            onChange={this.handleInputChange}
                                            /> Sismo <br></br>
                                            <input 
                                            type='checkbox' 
                                            name='sismo' 
                                            value='sismo'
                                            onChange={this.handleInputChange}
                                            /> Sismo <br></br>
                                            <input 
                                            type='checkbox' 
                                            name='sismo' 
                                            value='sismo'
                                            onChange={this.handleInputChange}
                                            /> Sismo <br></br>
                                            <input 
                                            type='checkbox' 
                                            name='otro' 
                                            value='otro'
                                            onChange={this.handleInputChange}                                            
                                            /> Otro <br></br>
                                            {this.renderInputField()}
                                        </div>
                                    </div>    
                                </div>
                                
                            </div>
    
                        
                    </div> 
                    </form>
                </div>
           </div>    
    
          </div>
          
        )
      }

}
export default Checklist