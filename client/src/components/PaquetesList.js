import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'reactstrap';
import './CSS/PaquetesList.css';

import { connect } from 'react-redux';
import { getViajes, resetSearch, filterViajes } from '../actions/viajeActions';
import PropTypes from 'prop-types';

import Berlin from './Thumbnails/berlin.jpg';

class PaquetesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
        
    }
}
  componentDidMount(){

  }
  componentWillMount() {
    //this.props.resetSearch();

  }

  onPaqueteClick = (ID_Viaje, e) => {
    console.log(ID_Viaje);
  }

  render() {
    const { viajesFiltered, searchtxt } = this.props.viaje;
    //const viajes = viajesFiltered.length === 0 ? this.props.viaje.viajes : viajesFiltered;
    const viajes = viajesFiltered.length === 0 && searchtxt === '' ? this.props.viaje.viajes : viajesFiltered;
    console.log(viajes);
    return (
        <Container className='ListBundles'>
          <div className="row">
          
            {viajes.map((viaje) => (
              <React.Fragment key={viaje.id_viaje}>
              <div className="col-sm-1 mb-4"></div>
              <div className="col-sm-3 mb-4">
                <a href={'/pk/' + viaje.id_viaje} onClick={this.onPaqueteClick.bind(this, viaje.id_viaje)}>
                <div className="Bundle">
                  <div className="Bundle-in-1">
                    <img className='img-1' src={viaje.thumbnail} alt='lol'></img>
                  </div>
                  <div className="Bundle-in-2">
                    <div className='txt-1'>
                      <div className='txt-1-1'>{viaje.nombre}</div>
                      <div className='txt-1-2'></div>
                      <div className='txt-1-3'>cupos restantes {viaje.cupos_restantes}</div>
                    </div>
                  </div>
                  <div className="Bundle-in-3">
                    <div className='txt-2'>
                      <div className='left-txt'>
                        <div className='left-txt-1'>Precio por adulto</div>
                        <div className='left-txt-2'>{viaje.precio_adulto}</div>
                      </div>
                      <div className='right-txt'>
                        <div className='right-txt-1'>Precio por menor</div>
                        <div className='right-txt-2'>{viaje.precio_menor}</div>
                      </div> 
                    </div>
                  </div>
                </div>
                </a>
              </div>
              </React.Fragment>
            ))}

          </div>
        </Container>
    )
  }
}

PaquetesList.propTypes = {
  getViajes: PropTypes.func.isRequired,
  resetSearch: PropTypes.func.isRequired,
  filterViajes: PropTypes.func.isRequired,
  viaje: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({ //mapping redux state to component property
  viaje: state.viaje
});

export default connect(mapStateToProps, { getViajes, resetSearch, filterViajes })(PaquetesList);