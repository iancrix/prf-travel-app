import React, { Component } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Container
} from 'reactstrap';
import './CSS/AppNavbar.css';

class AppNavBar extends Component {
  state = {
    isOpen: false
  }

  toggle = () => {
    this.setState({
        isOpen: !this.state.isOpen
    });
  }
 
  render(){
      return (
        <div>
        <Navbar color='navbar navbar-dark bg-dark' dark expand="sm" className="mb-5">
            <Container className='container-1'>
            <img className='brand-img' src="https://cdn4.iconfinder.com/data/icons/world-travel-guide/512/travel-13-512.png" width="60" height="60" alt=""></img>
                <NavbarBrand href="/" className='navbrand-1'>
                  PRF TRAVEL</NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                      <NavItem className='navitem-1'>
                        <NavLink href="/">
                          PAQUETES
                        </NavLink>
                      </NavItem>
                        <NavItem className='navitem-1'>
                          <NavLink href="/">
                              GALERÍA
                          </NavLink>
                        </NavItem>
                        <NavItem className='navitem-1'>
                          <NavLink href="/contact">
                           CONTACTO
                          </NavLink>
                        </NavItem>
                      </Nav>
                </Collapse>
            </Container>
          </Navbar>
      </div>
    
          );
  }
}

export default AppNavBar;