import React, { Component } from 'react'
import './CSS/Newsletter.css';

class Newsletter extends Component {
  constructor (props){
    super(props);
    this.botonoprimido1=React.createRef();

  }
    botonoprimido(){
      this.botonoprimido1.current.classList.add("open");
}
   myFunction() {
    this.botonoprimido1.current.classList.remove("open");
  }
  render() {
    return (
            <div>
        <div className="button"onClick={this.botonoprimido.bind(this)}>
          <button><span>Suscribete</span></button>
        </div>
        <div className="pop-up"ref={this.botonoprimido1}>
          <div className="content">
            <div className="container">
              <div className="dots">
                <div className="dot" />
                <div className="dot" />
                <div className="dot" />
              </div>
              <span className="close"onClick={this.myFunction.bind(this)} >CERRAR</span>
              <div className="title">
                <h1>Suscribete</h1>
              </div>
              <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/256492/cXsiNryL.png" alt="Car" />
              <div className="Suscribete">
                <h1>¡Suscribete para informarte de los últimos paquetes!<span> </span></h1>
                <form>
                  <input type="email" placeholder="Correo electronico" />
                  <input type="submit" defaultValue="Subscribe" />
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Newsletter;
