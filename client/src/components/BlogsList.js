import React, {Component} from 'react'
import './CSS/BlogsList.css'
import axios from 'axios'
import httpsProxyAgent from 'https-proxy-agent'
import {Button} from 'reactstrap'

class BlogPreview extends Component{
  constructor(props){
    super(props)
    console.log(props.blog)
    this.state={...props}
    
  }

  clicked(){
    window.location = `/blog/${this.state.blog.id}`
  }

  render(){
    return (
      <div className="blog-min-body" onClick={this.clicked.bind(this)}>
        <img className="img-fondo-min-blog" src={this.state.blog.thumbnail}/>
        <div className="blog-min-text">
          <h3 className="blog-min-title">{this.state.blog.titulo}</h3><br/>
          <p className="blog-min-content">{this.state.blog.contenido}...</p>
        </div>
      </div>
    )
  }
}


class BlogsList extends Component{
  constructor(props){
    super(props)
    this.state={}
  }

  nuevoPost() {
    window.location = `/newPost`
  }

  componentDidMount(){
    var agent = new httpsProxyAgent('http://localhost:5000');

    var config = {
      url: `/api/viajes/allblogs`,
      httpsAgent: agent,
      method: 'get'
    }

    axios.request(config)
      .then((res) => {
        this.setState({
          blogs: res.data[0]
        })
      })
      .catch(err => {
        console.log(err)
        this.setState({
          error: true
        })
      })
  }

  render(){
    return this.state.blogs 
          ?(<div className="full-blogs">
              <div className="title-blogs">
                <h1>Blog</h1>
              </div>
              
              <div className="body-blogs">
                <div>
                  <Button color="primary" onClick={this.nuevoPost.bind(this)}>Nuevo Post</Button>
                </div>
                {this.state.blogs.map((blog) => (<BlogPreview blog={blog} />))}
              </div>
            </div>)
          :<h1>No hay viajes</h1>
  }
}



export default BlogsList