import React, { Component } from 'react'
import './CSS/BlogEntry.css'
import axios from 'axios'
import httpsProxyAgent from 'https-proxy-agent';
//import {connect} from 'react-redux'
//import { getViajes } from '../actions/viajeActions';
//import PropTypes from 'prop-types';

//import { getPost } from '../actions/viajeActions'
//const getPost = require('../actions/viajeActions').getPost

class BlogEntry extends Component{
  constructor(props){
    super(props);
    this.state = {title:"Titulo",autor:"Autor",id:props.match.params.id,geterror:false};
    //console.log(props);
  }

  componentDidMount(){
    //proxy, no se puede hacer consulta directa debido a restriccion de comunicacion entre puertos
    var agent = new httpsProxyAgent('http://localhost:5000'); 

    var config = {
      url: `/api/viajes/blog/${this.state.id}`,
      httpsAgent: agent,
      method: 'get'
    }

    axios.request(config)
        .then((res) => {
          var newdata = res.data[0][0]
          this.setState({
            title:newdata.titulo,
            autor: newdata.autor,
            thumbnail: newdata.thumbnail,
            text: newdata.contenido
          })
        })
        .catch(err => {
          console.log(err)
          this.setState({
            error: true
          })
        })

    //this.props.getViajes();
    //axios.get('api/viajes/DAOMySQL').then(console.log).catch((err)=>console.log(err))
    //console.log(`hello this is your id: ${this.state.id}`)
    /*axios.get('http://localhost:5000/api/viajes/blog/1', {
      headers: { 'Access-Control-Allow-Origin': '*' },
      crossdomain: true
    }).then(console.log).catch((err)=>{console.log(err)});*/
  }

  render() {
    console.log(this.props);
    return (
      <div className="div-total-total">
        <div className="div-total">
          <div><span className="post-title" >{this.state.title}</span><br/> by <span className="post-autor">{this.state.autor}</span></div><br />
          <img className="post-thumbnail" src={this.state.thumbnail} alt="" />
          <p >{this.state.text}</p>
        </div>
      </div>
    )
  }
}

/*BlogEntry.propTypes = {
  getPost: PropTypes.func.isRequired,
/*  viaje: PropTypes.object.isRequired*/
/*}

const mapStateToProps = (state) => ({ //mapping redux state to component property
  ...state
});

export default connect(mapStateToProps, { getPost })(BlogEntry);
*/
/*BlogEntry.propTypes = {
  getViajes: PropTypes.func.isRequired,
  viaje: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({ //mapping redux state to component property
  viaje: state.viaje
});
*/
export default BlogEntry