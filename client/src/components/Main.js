import  React, { Component } from 'react'
import './CSS/Main.css';
class Main extends Component {
    render() {
        return (
            <div>
        <header>
          <h1>SITE TITLE</h1>
          <nav>
            <ul>
              <li><a href="#">HOME</a></li>
              <li><a href="#">ABOUT</a></li>
              <li><a href="#">SERVICES</a></li>
              <li><a href="#">CONTACT</a></li>
            </ul>
          </nav>
        </header>
        <section className="main-slider">
          <div className="item image">
            <span className="loading">Loading...</span>
            <figure>
              <div className="slide-image slide-media" style={{backgroundImage: 'url("https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLRkY4S0JDTk1BbE0")'}}>
                <img data-lazy="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLRkY4S0JDTk1BbE0" className="image-entity" />
              </div>
              <figcaption className="caption">Static Image</figcaption>
            </figure>
          </div>
          <div className="item vimeo" data-video-start={4}>
            <iframe className="embed-player slide-media" src="https://player.vimeo.com/video/217885864?api=1&byline=0&portrait=0&title=0&background=1&mute=1&loop=1&autoplay=0&id=217885864" width={980} height={520} frameBorder={0} webkitallowfullscreen='true' mozallowfullscreen allowFullScreen />
            <p className="caption">Vimeo</p>
          </div>
          <div className="item image">
            <figure>
              <div className="slide-image slide-media" style={{backgroundImage: 'url("https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLNXBIcEdOUFVIWmM")'}}>
                <img data-lazy="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLNXBIcEdOUFVIWmM" className="image-entity" />
              </div>
              <figcaption className="caption">Static Image</figcaption>
            </figure>
          </div>
          <div className="item youtube">
            <iframe className="embed-player slide-media" width={980} height={520} src="https://www.youtube.com/embed/QV5EXOFcdrQ?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&playlist=QV5EXOFcdrQ&start=1" frameBorder={0} allowFullScreen /> 
            <p className="caption">YouTube</p>
          </div>
          <div className="item image">
            <figure>
              <div className="slide-image slide-media" style={{backgroundImage: 'url("https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSlBkWDBsWXJNazQ")'}}>
                <img data-lazy="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSlBkWDBsWXJNazQ" className="image-entity" />
              </div>
              <figcaption className="caption">Static Image</figcaption>
            </figure>
          </div>
          <div className="item video">
            <video className="slide-video slide-media" loop muted preload="metadata" poster="https://drive.google.com/uc?export=view&id=0B_koKn2rKOkLSXZCakVGZWhOV00">
              <source src="https://player.vimeo.com/external/138504815.sd.mp4?s=8a71ff38f08ec81efe50d35915afd426765a7526&profile_id=112" type="video/mp4" />
            </video>
            <p className="caption">HTML 5 Video</p>
          </div>
        </section>
        <section className="container">
          <div className="content">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit veniam quisquam, rem illum dicta cumque, voluptate fugiat impedit beatae rerum ratione, voluptates nisi magni delectus ab, eaque atque iste. Molestias incidunt nemo veniam alias nam nisi distinctio optio error architecto odit! Illo dicta nulla fugiat distinctio laudantium, corrupti eum unde.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit veniam quisquam, rem illum dicta cumque, voluptate fugiat impedit beatae rerum ratione, voluptates nisi magni delectus ab, eaque atque iste. Molestias incidunt nemo veniam alias nam nisi distinctio optio error architecto odit! Illo dicta nulla fugiat distinctio laudantium, corrupti eum unde. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit veniam quisquam, rem illum dicta cumque, voluptate fugiat impedit beatae rerum ratione, voluptates nisi magni delectus ab, eaque atque iste. Molestias incidunt nemo veniam alias nam nisi distinctio optio error architecto odit! Illo dicta nulla fugiat distinctio laudantium, corrupti eum unde.</p>
          </div>
        </section>
      </div>
        )
    }
}

export default Main;
