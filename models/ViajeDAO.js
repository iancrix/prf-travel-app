const MongoClient = require('mongodb').MongoClient;
const mysql = require('mysql');
const keys = require('../config/keys');
const execQuery = require('./BaseDAO').execQuery;

function getAllItemsMySQL() {
    return execQuery('CALL getAllViajes()',[]);
}

function getItemMySQL(id) {
    return execQuery('CALL getViajePorId(?)',id);
}

function getViajeByCiudadMySQL(viajeid){
  return execQuery('CALL buscarViajePorCiudad(?)', viajeid);
}

function getCiudadByViajeMySQL(ciudadid){
  return execQuery('CALL buscarCiudadPorViaje(?)', ciudadid);
}

function comprarViajeMySQL(viajeid,clienteid) {
  return execQuery('CALL comprarViaje(?,?)', [viajeid, clienteid]);
}

function getAllCiudadesMySQL(){
  return execQuery('CALL getAllCiudades()');
}

function getAllItemsMongoDB() {
  return new Promise((callback) => {
    MongoClient.connect(keys.mongoURI, { useNewUrlParser: true })
      .then(function (client, err) {
        console.log("Connecting to MongoDB...");
        if (err) { throw err; }
        const db = client.db('PRFTravelDB');
        db.collection('viajes')
          .find() 
          .toArray() //Promise con info sobre los resultados de consulta
          .then(items => { callback(items) })  
        client.close();
      });
  });
}

function getCiudadesByPregunta(pregunta){
  throw Error('No implementado');
}

module.exports.getAllItemsMongoDB = getAllItemsMongoDB;
module.exports.getAllItemsMySQL = getAllItemsMySQL;
module.exports.getItemMySQL = getItemMySQL;
module.exports.getViajeByCiudadMySQL = getViajeByCiudadMySQL;
module.exports.getCiudadByViajeMySQL = getCiudadByViajeMySQL;
module.exports.getAllCiudadesMySQL = getAllCiudadesMySQL;
module.exports.comprarViajeMySQL = comprarViajeMySQL;
module.exports.getCiudadesByPregunta = getCiudadesByPregunta;