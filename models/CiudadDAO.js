const MongoClient = require('mongodb').MongoClient;
const mysql = require('mysql');
const keys = require('../config/keys');

function execQuery(query,args){
    return new Promise((resolve,reject)=>{
      console.log("Processing query...");
      const conn = mysql.createConnection(keys.mySQLURI);
      conn.connect();
      conn.query(query,args,(err,rows,fields)=>{
        if (err){
          reject(err);
        } else {
          resolve(rows);
        }
      })
      conn.end((err)=>{});
      console.log("Finished");
    })
  }
  
function getAllCiudadesMySQL() {
    return execQuery('CALL getAllCiudades()',[]);
}

module.exports.getAllCiudadesMySQL = getAllCiudadesMySQL;