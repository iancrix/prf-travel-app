const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const ViajeSchema = new Schema({
    nombre:{
      type: String,
      required: true
    },
    origen:{
      type: String,
      default: 'Bogota'
    },
    destino:{
      type: String,
      default: 'New York'
    },
    fecha_inicio:{
      type: Date,
      default: Date.now
    },
    fecha_fin:{
      type: Date,
      default: Date.now
    }
});

module.exports = Viaje = mongoose.model('viaje', ViajeSchema);