const mysql = require('mysql');
const keys = require('../config/keys');


function execQuery(query, args) {
  return new Promise((resolve, reject) => {
    console.log("Processing query...");
    const conn = mysql.createConnection(keys.mySQLURI);
    conn.connect();
    conn.query(query, args, (err, rows, fields) => {
      if (err) {
        reject(err);
      } else {
        resolve(rows);
      }
    })
    conn.end((err) => { });
    console.log("Finished");
  })
}

module.exports.execQuery = execQuery;