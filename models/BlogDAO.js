const mysql = require('mysql');
const keys = require('../config/keys');
const execQuery = require('./BaseDAO').execQuery;

class BlogDAO{
  static getAllBlogs(){
    return execQuery('CALL getAllBlogs()');
  }
  static getAllBlogsShort() {
    return execQuery('CALL getAllBlogsShort()');
  }
  static getBlogPorId(id) {
    return execQuery('CALL getBlogPorId(?)',id);
  }
  static postNewBlog(titulo,autor,contenido,thumbnail){
    return execQuery('CALL postNewBlog(?,?,?,?)',[titulo,autor,contenido,thumbnail])
  }
}

module.exports = BlogDAO;