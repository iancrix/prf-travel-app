const express = require('express'); // Backend Framework
const mongoose = require('mongoose'); // ORM MongoDB Database
const keys = require('./config/keys');
const mysql = require('mysql');

const viajes = require('./routes/api/viajes');
const ciudades = require('./routes/api/ciudades');

const app = express(); //Initialize express on a variable

// Bodyparser Middleware

//app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded())

// DB Config
const db = require('./config/keys').mongoURI;

// Connect to Mongo
/*
mongoose
.connect(db, {useNewUrlParser: true})
.then(() => console.log('MongoDB Connected...'))
.catch(err => console.log(err));
*/
// Connect to MySQL Test
/* console.log("Trying to connect to MySQL...")
var connection = mysql.createConnection(keys.mySQLURI);

connection.connect();
console.log('MySQL Connected...')

connection.end((err) => { });
console.log("Connection Finished..."); */

// Use Routes
// Any request that goes to api/viajes/*  -- it will go to the file variable (viajes)
app.use('/api/viajes', viajes);
app.use('/api/ciudades', ciudades);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));