const express = require('express');
const router = express.Router();
const ciudadDAO = require('../../models/CiudadDAO');

// @route GET api/ciudades/DAOMySQL
router.get('/DAOMySQL', (req, res) => {
    ciudadDAO.getAllCiudadesMySQL()
      .then((ciudades) => res.json(ciudades));   
  })

module.exports = router;