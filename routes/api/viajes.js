const express = require('express');
const router = express.Router();
const viajeDAO = require('../../models/ViajeDAO');
const blogDAO = require('../../models/BlogDAO');
const chatbot = require('../../controllers/ChatbotController');


router.get('/DAOMongo',(req,res)=>{
  viajeDAO.getAllItemsMongoDB()
    .then((viajes)=>res.json(viajes));
})

// @route GET api/viajes/DAOMySQL
router.get('/DAOMySQL', (req, res) => {
  viajeDAO.getAllItemsMySQL()
    .then((viajes) => res.json(viajes));   
})

// @route GET api/viajes/DAOMySQL/:id
router.get('/DAOMySQL/:id', (req, res) => {
  viajeDAO.getItemMySQL(req.params.id)
    .then((viaje) => res.json(viaje));   
})

// @route GET api/viajes/DAOMySQL2/:id
router.get('/DAOMySQL2/:id', (req, res) => {
  viajeDAO.getViajeByCiudadMySQL(req.params.id)
    .then((viaje) => res.json(viaje));   
})

// @route GET api/viajes/DAOMySQL3/:id
router.get('/DAOMySQL3/:id', (req, res) => {
  viajeDAO.getCiudadByViajeMySQL(req.params.id)
    .then((ciudad) => res.json(ciudad));   
})



router.get('/blog/:id', (req, res) => {
  blogDAO.getBlogPorId(req.params.id)
    .then((viaje) => res.json(viaje));
})

router.get('/allblogs', (req, res) => {
  blogDAO.getAllBlogsShort()
    .then((viaje) => res.json(viaje));
})

router.post('/blog/newpost',(req,res)=>{
  console.log(req.body)
  const {titulo, autor, contenido, thumbnail} = req.body

  console.log("\n\n\ntodo bien por aca\n\n\n")
  console.log(titulo)
  console.log(autor)
  console.log(contenido)
  console.log(thumbnail)
  blogDAO.postNewBlog(titulo,autor,contenido,thumbnail).then((nothing)=>res.json({autor:"autor",...req.body}));
})


router.get('/chatbot',(req,res)=>{
  console.log("\n\n\n")
  console.log(req.query)
  console.log("\n\n\n")
  //res.json(req);return
  chatbot.responderChatbot(req.query.mensaje)
    .then((rows)=>res.json(rows))
    .catch((err)=>{console.log(err);res.json({mensaje:"<h1>No entiendo lo que quieres decir</h1>"})});
})

// Viaje Model
const Viaje = require('../../models/Viaje');

// @route GET api/viajes
// @desc GET ALL Viajes
// @access Public
router.get('/', (req, res) => {
  Viaje.find()
    .sort({ fecha_inicio: -1 })
    .then(viajes => res.json(viajes));
});

// @route POST api/viajes
// @desc Create a Viaje
// @access Public
router.post('/', (req, res) => {
  const newViaje = new Viaje({
      nombre: req.body.nombre,
      origen: req.body.origen,
      destino: req.body.destino
  });
  
  newViaje.save().then(viaje => res.json(viaje));
});

// @route DELETE api/viajes/:id
// @desc Delete a Viaje
// @access Public
router.delete('/:id', (req, res) => {
    Viaje.findById(req.params.id)
      .then(viaje => viaje.remove().then(() => res.json({success: true})))
      .catch(err => res.status(404).json({success: false}));
  });
  

module.exports = router;