/*CREATE TABLE `bajas` (
  `ID_Admin` CHAR(4),
  `fecha_baja` DATE,
  KEY `FK` (`ID_Admin`)
);

CREATE TABLE `mis_planes` (
  `ID_cliente` CHAR(8),
  `ID_viaje` INT,
  `valido` BOOLEAN,
  `viajeros` INT,
  KEY `FK` (`ID_cliente`, `ID_viaje`)
);

CREATE TABLE `Ciudad_Evento` (
  `ID_Ciudad` int,
  `ID_Evento` int,
  KEY `FK` (`ID_Ciudad`, `ID_Evento`)
);

CREATE TABLE `Viaje` (
  `ID_Viaje` int,
  `ID_Hotel` int,
  `ID_Aerolinea` int,
  `Origen` Ciudad,
  `Destino` Ciudad,
  `Fecha_inicio` DATE,
  `Fecha_fin` DATE,
  `Precio_adulto` double,
  `Precio_menor` double,
  `Cupos_restantes` int,
  `Equipaje` CHAR(2),
  PRIMARY KEY (`ID_Viaje`),
  KEY `FK` (`ID_Hotel`, `ID_Aerolinea`)
);

CREATE TABLE `Hotel` (
  `ID_Hotel` int,
  `Nombre` VARCHAR(255),
  `Descripcion` VARCHAR(255),
  `Direccion` VARCHAR(255),
  `Puntuacion` float,
  `Url` VARCHAR(255),
  PRIMARY KEY (`ID_Hotel`)
);

CREATE TABLE `Administrador` (
  `ID_Admin` CHAR(4),
  `contra_SI` VARCHAR(16),
  `nombre` VARCHAR(30),
  `apellidos` VARCHAR(3),
  `edad` INT,
  `cc` VARCHAR(15),
  `fecha_nacimiento` DATE,
  `fecha_ingreso` DATE,
  PRIMARY KEY (`ID_Admin`)
);

CREATE TABLE `Url_Imagenes` (
  `ID_Ciudad` int,
  `ID_URL` int,
  `Url` VARCHAR(255),
  KEY `FKve` (`ID_Ciudad`),
  KEY `Key` (`ID_URL`)
);

CREATE TABLE `Aerolínea` (
  `ID_Aerolínea` int,
  `Nombre` VARCHAR(255),
  `Descripción` VARCHAR(1000),
  PRIMARY KEY (`ID_Aerolínea`)
);

CREATE TABLE `Cliente` (
  `ID_cliente` CHAR(8),
  `Nombre` VARCHAR(255),
  `Contra_SI` VARCHAR(255),
  `Correo` VARCHAR(255),
  PRIMARY KEY (`ID_cliente`)
);

CREATE TABLE `Evento` (
  `ID_Evento` int,
  `Nombre` VARCHAR(255),
  `Fecha_inicio` DATE,
  `Fecha_fin` DATE,
  `Precio` double,
  PRIMARY KEY (`ID_Evento`)
);

CREATE TABLE `Ciudad` (
  `ID_Ciudad` int,
  `Nombre` Varchar(255),
  `Descripcion` Varchar(1000),
  PRIMARY KEY (`ID_Ciudad`)
);
*/


CREATE TABLE Cliente(
    ID_Cliente int primary key,
    Nombre varchar(255),
    Correo varchar(255),
    Celular varchar(20),
    Clave varchar(255)
);
CREATE TABLE Aerolinea(
    ID_Aerolinea int primary key,
    Nombre varchar(255),
    Correo varchar(255),
    Webpage varchar(255)
);
CREATE TABLE Viaje(
    ID_Viaje int primary key,
    Nombre varchar(100),
    Cupos_restantes int,
    Precio_menor decimal(12,2),
    Precio_adulto decimal(12,2),
    Fecha_inicio datetime,
    Fecha_fin datetime,
    ID_Aerolinea int,
    Descripcion text,
    thumbnail varchar(500),
    foreign key(ID_Aerolinea) references Aerolinea(ID_Aerolinea)
);
CREATE TABLE Ciudad(
    ID_Ciudad int primary key,
    Nombre varchar(50),
    Thumbnail varchar(500),
    Informacion text
);
CREATE TABLE Evento(
    ID_Evento int primary key,
    Nombre VARCHAR(255),
    Fecha_inicio datetime,
    Fecha_fin datetime,
    Precio double,
    ID_Ciudad int,
    foreign key(ID_Ciudad) references Ciudad(ID_Ciudad)
);


CREATE TABLE Hotel(
    ID_Hotel int primary key,
    Nombre varchar(255),
    Descripcion text,
    Puntuacion float,
    Webpage varchar(500),
    ID_Ciudad int,
    foreign key(ID_Ciudad) references Ciudad(ID_Ciudad)
);
CREATE TABLE ViajeCiudad(
    ID_Viaje int,
    ID_Ciudad int,
    ID_Hotel int,
    foreign key(ID_Viaje) references Viaje(ID_Viaje),
    foreign key(ID_Ciudad) references Ciudad(ID_Ciudad),
    foreign key(ID_Hotel) references Hotel(ID_Hotel)
    
);
CREATE TABLE ViajeCliente(
    ID_Viaje int,
    ID_Cliente int,
    foreign key(ID_Viaje) references Viaje(ID_Viaje),
    foreign key(ID_Cliente) references Cliente(ID_Cliente)
);

DELIMITER $$
create procedure getAllViajes()
BEGIN
    select id_viaje, nombre, descripcion, thumbnail, 
    cupos_restantes, precio_menor, precio_adulto,
    fecha_inicio, fecha_fin, id_aerolinea from Viaje;
END$$



DELIMITER $$
CREATE procedure getAllViajes()
BEGIN
select id_viaje, nombre, cupos_restantes, precio_menor, precio_adulto, fecha_inicio,
    fecha_fin, id_aerolinea, descripcion, thumbnail from Viaje;
END$$

DELIMITER $$
create procedure getViajeporId(id int)
BEGIN
    select id_viaje, nombre, descripcion, thumbnail, 
    cupos_restantes, precio_menor, precio_adulto,
    fecha_inicio, fecha_fin, id_aerolinea from Viaje where id_viaje = id;
END$$

DELIMITER $$
create procedure createViaje(nID_Viaje int,
							nNombre varchar(100),
							nCupos_restantes int,
							nPrecio_menor decimal(12,2),
							nPrecio_adulto decimal(12,2),
							nFecha_inicio datetime,
							nFecha_fin datetime,
							nID_Aerolinea int,
							nDescripcion text,
							nthumbnail varchar(500))
BEGIN
    insert into viaje( id_viaje, nombre, cupos_restantes,   
     precio_menor, precio_adulto,fecha_inicio, fecha_fin,
     id_aerolinea,descripcion, thumbnail) values 
     (nid_viaje, nnombre, ncupos_restantes,   
     nprecio_menor, nprecio_adulto,nfecha_inicio, nfecha_fin,
     nid_aerolinea,ndescripcion, nthumbnail);
     
END$$


DELIMITER $$
create procedure createEvento(nID_Evento int,
							nNombre VARCHAR(255),
							nFecha_inicio datetime,
							nFecha_fin datetime,
							nPrecio double,
							nID_Ciudad int)
BEGIN
    insert into evento(ID_Evento,Nombre,Fecha_inicio,
			Fecha_fin,Precio,ID_Ciudad) values 
			(nID_Evento,nNombre,nFecha_inicio,nFecha_fin,
			nPrecio,nID_Ciudad);
     
END$$


DELIMITER $$
create procedure createCiudad(nID_Ciudad int,
							nNombre varchar(50),
							nThumbnail varchar(500),
							nInformacion text)
BEGIN
    insert into ciudad(ID_Ciudad,Nombre,Thumbnail,Informacion) values 
			(nID_Ciudad,nNombre,nThumbnail,nInformacion);
     
END$$


DELIMITER $$
create procedure createAerolinea(nID_Aerolinea int,
								nNombre varchar(255),
								nCorreo varchar(255),
								nWebpage varchar(255))
BEGIN
    insert into aerolinea(ID_Aerolinea,Nombre,Correo,Webpage)
    values (nID_Aerolinea,nNombre,nCorreo,nWebpage);
END$$


DELIMITER $$
create procedure createCliente(nID_Cliente int,
							nNombre varchar(255),
							nCorreo varchar(255),
							nCelular varchar(20),
							nClave varchar(255))
BEGIN
    insert into cliente(ID_Cliente,Nombre,Correo,Celular,Clave)
    values (nID_Cliente,nNombre,nCorreo,nCelular,nClave);
END$$

DELIMITER $$
create procedure agregarParada(nID_Viaje int,
							nID_ciudad int,
							nID_hotel int)
BEGIN
    insert into viajeciudad(id_viaje,id_ciudad,id_hotel)
    values (nid_viaje,nid_ciudad,nid_hotel);
END$$

DELIMITER $$
create procedure comprarViaje(nID_Viaje int,
							nID_Cliente int)
BEGIN
    insert into viajecliente(id_viaje, id_cliente)
    values (nid_viaje,nid_cliente);
END$$

DELIMITER $$
create procedure buscarViajePorCiudad(nID_Ciudad int)
BEGIN
    select V.id_viaje, nombre, descripcion, thumbnail, 
    cupos_restantes, precio_menor, precio_adulto,
    fecha_inicio, fecha_fin, id_aerolinea from Viaje as V, ViajeCiudad as VC
    where V.id_viaje = VC.id_viaje and VC.id_ciudad = nid_ciudad;
END$$

--Para llenar la base de datos

--Aerolineas
insert into aerolinea(ID_Aerolinea,Nombre,Correo,Webpage)
values (1,'Avianca','avianca@gmail.com','https://www.avianca.com/co/en/');

insert into aerolinea(ID_Aerolinea,Nombre,Correo,Webpage)
values (2,'lATAM Airlines','latam@gmail.com','https://www.latam.com/en_un/');

insert into aerolinea(ID_Aerolinea,Nombre,Correo,Webpage)
values (3,'Copa Airlines','copa@gmail.com','https://www.copaair.com/en/web/us');

insert into aerolinea(ID_Aerolinea,Nombre,Correo,Webpage)
values (4,'American Airlines','america@gmail.com','https://www.aa.com/homePage.do?locale=es_CO');

insert into aerolinea(ID_Aerolinea,Nombre,Correo,Webpage)
values (5,'Delta Airlines','delta@gmail.com','https://www.delta.com/');

--Ciudades
insert into ciudad(ID_Ciudad,Nombre,Thumbnail,Informacion) 
values (1,'Orlando','http://babiestravellite.com/wp-content/uploads/2015/08/IOA_Universal.jpg','Start planning your dream vacation to the No. 1 Summer Travel Destination as ranked by AAA! Orlando is gearing up for an unforgettable summer packed with fresh experiences. Find theme parks from Walt Disney World® Resort and Universal Orlando Resort to SeaWorld® Orlando and LEGOLAND® Florida Resort. Discover attractions you can’t experience anywhere else.');

insert into ciudad(ID_Ciudad,Nombre,Thumbnail,Informacion) 
values (2,'Paris','https://europeanbusinessmagazine.com/wp-content/uploads/2017/07/paris.jpg','Paris monument-lined boulevards, museums, classical bistros and boutiques are enhanced by a new wave of multimedia galleries, creative wine bars, design shops and tech start-ups.');

insert into ciudad(ID_Ciudad,Nombre,Thumbnail,Informacion) 
values (3,'Berlin','https://imgc.allpostersimages.com/img/print/posters/michele-falzone-germany-berlin-alexanderplatz-tv-tower-fernsehturm_a-G-8950034-4990875.jpg','Berlin combo of glamour and grit is bound to mesmerise all those keen to explore its vibrant culture, cutting-edge architecture, fabulous food, intense parties and tangible history.');

insert into ciudad(ID_Ciudad,Nombre,Thumbnail,Informacion) 
values (4,'New York','https://amp.businessinsider.com/images/58dbdc86dd0895d16f8b4700-750-562.jpg','There is nothing like New York City during the holidays. Outdoor skating rinks and illuminated streets, people warming their hands with mugs of cocoa and hot apple cider, animated and decorated shop windows... no wonder so many holiday films are set here!');

insert into ciudad(ID_Ciudad,Nombre,Thumbnail,Informacion) 
values (5,'Praga','https://i1.wp.com/mevoyalmundo.com/wp-content/uploads/2017/02/trabajar-vivir-praga.jpg?fit=750%2C460&ssl=1','Prague, City of a Hundred Spires, a UNESCO monument and one of the most beautiful cities in the world. Get to know it in person!');

insert into ciudad(ID_Ciudad,Nombre,Thumbnail,Informacion) 
values (6,'Madrid','https://www.stickerforwall.com/22651-thickbox/photo-wall-murals-madrid-city-financial.jpg','Madrid is a beguiling place with an energy that carries one simple message: this city really knows how to live.');

insert into ciudad(ID_Ciudad,Nombre,Thumbnail,Informacion) 
values (7,'London','https://www.telegraph.co.uk/content/dam/property/2017/02/16/JS120777801-London-Views-xlarge_trans_NvBQzQNjv4Bqek9vKm18v_rkIPH9w2GMNpPHkRvugymKLtqq96r_VP8.jpg', 'One of the world most visited cities, London has something for everyone: from history and culture to fine food and good times.');

insert into ciudad(ID_Ciudad,Nombre,Thumbnail,Informacion) 
values (8,'Bogota','http://hellomoto.com.co/wp/wp-content/uploads/2018/04/hellobogota_1-1024x687.png', 'Bogotá is the capital and largest city in Colombia. A melting pot of people from around the country, it is diverse and multicultural, with a blend of modern and colonial architecture.');

--Viajes
insert into viaje( id_viaje, nombre, cupos_restantes,   
     precio_menor, precio_adulto,fecha_inicio, fecha_fin,
     id_aerolinea,descripcion, thumbnail) 
values (1, 'Vive la France', 13,   
     272100, 505499,'2019-06-02', '2019-06-19',
     5,'As one of the largest countries in Europe, it can be difficult to take the entire country of France in, but it is possible to travel to France and enjoy the best of its culture. Those who visit France will find it has a varied topography, which includes everything from the French Alps to the sandy beaches of the Mediterranean, offering visitors a chance to personally design their trip.', 
     'https://www.telegraph.co.uk/content/dam/Travel/hotels/europe/france/paris/paris-cityscape-overview-guide.jpg?imwidth=450');

insert into viaje( id_viaje, nombre, cupos_restantes,   
     precio_menor, precio_adulto,fecha_inicio, fecha_fin,
     id_aerolinea,descripcion, thumbnail) values 
     (2, 'I Love NY', 9,   
     394299, 471820,'2019-06-05', '2019-07-01',
     4,'Discover the many wonders that make New York the country most desirable vacation destination and enjoy some of the many fun and exciting things to do here. Experience the endless treasures that New York State has to offer.', 
     'https://imgc.allpostersimages.com/img/print/u-g-Q12SYR60.jpg?w=550&h=550&p=0');

insert into viaje( id_viaje, nombre, cupos_restantes,   
     precio_menor, precio_adulto,fecha_inicio, fecha_fin,
     id_aerolinea,descripcion, thumbnail) values 
     (3, 'Magic Holiday', 27,   
     753050, 932900,'2019-06-17', '2019-07-15',
     1 ,'It is so easy to get caught up in Greater Orlando – in the isolated, fabricated worlds of Disney or Universal Orlando (for which, lets face it, you are probably here) – that you forget all about the downtown city of Orlando itself. It has a lot to offer: lovely tree-lined neighborhoods; a rich performing arts and museum scene; several fantastic gardens and nature preserves; fabulous cuisine; great craft cocktails; and a delightfully slower pace devoid of manic crowds.', 
     'https://images2.kenwoodtravel.co.uk/thumbs/798x464/gallery/Orlando-Disney.jpg');

insert into viaje( id_viaje, nombre, cupos_restantes,   
     precio_menor, precio_adulto,fecha_inicio, fecha_fin,
     id_aerolinea,descripcion, thumbnail) values 
     (4, 'Tea with the Queen', 5,   
     270400, 568050,'2019-07-12', '2019-08-21',
     3,'England is full of pageantry and tradition, and nowhere else is that on display so vividly than in London. Yet the capital of the United Kingdom is hardly living in the past, continually courting innovation and growth in its art, architecture, restaurants, and hotels.', 
     'https://cdn.londonandpartners.com/study/assets/campaign-pages/76119-640x360-luip-general-why-london.jpg');

insert into viaje( id_viaje, nombre, cupos_restantes,   
     precio_menor, precio_adulto,fecha_inicio, fecha_fin,
     id_aerolinea,descripcion, thumbnail) values 
     (5, 'Ultimate tour', 15,   
     314500, 580320,'2019-05-30', '2019-07-06',
     4,'From east to west, Europe is a mixture of cultures, languages, heritage, architecture, and customs all across the continent. The dissolution of border checks within the EU means that travel in Europe has never been easier, more affordable or more convenient!', 
     'https://en3dias.net/wp-content/uploads/2018/09/en-cuantos-dias-se-ve-varsovia.jpg');

--Hoteles
insert into hotel(ID_Hotel,Nombre,Descripcion,Puntuacion,Webpage,ID_Ciudad)
values (1,'The Ludlow Hotel','Este hotel ofrece vistas espectaculares de los rascacielos y puentes de la ciudad de Nueva York, y está situado en el barrio de Lower East Side de Manhattan. El establecimiento proporciona WiFi gratuita en las habitaciones.',9.1,'http://ludlowhotel.com/',4);

insert into hotel(ID_Hotel,Nombre,Descripcion,Puntuacion,Webpage,ID_Ciudad)
values (2,'MADE Hotel','El MADE Hotel se encuentra en Nueva York, a 600 metros del Empire State Building, y dispone de terraza. Este hotel de 4 estrellas también cuenta con bar, WiFi gratuita y recepción las 24 horas.',9.0,'https://www.madehotels.com/',4);

insert into hotel(ID_Hotel,Nombre,Descripcion,Puntuacion,Webpage,ID_Ciudad)
values (3,'Z Hotel Covent Garden','El hotel Z Covent Garden ofrece 113 habitaciones y se encuentra en pleno West End de Londres. Hay WiFi gratuita en todo el hotel, incluidas las zonas de recepción y cafetería de la planta baja.',9.0,'https://www.thezhotels.com/hotels/covent-garden/',7);

insert into hotel(ID_Hotel,Nombre,Descripcion,Puntuacion,Webpage,ID_Ciudad)
values (4,'Drury Inn & Suites Orlando','Este hotel de Orlando, Florida, está junto a la salida 74A de la carretera interestatal 4, en la zona de sitios para comer Restaurant Row, a 2 km del Universal Studios Orlando. Este Drury Inn es un hotel asociado Universal Partner Hotel, así que ofrece entradas y paquetes para parques temáticos.',9.1,'https://www.druryhotels.com/locations/orlando-fl/drury-inn-and-suites-orlando',1);

insert into hotel(ID_Hotel,Nombre,Descripcion,Puntuacion,Webpage,ID_Ciudad)
values (5,'SpringHill Suites Marriot','El SpringHill Suites se encuentra a 1,2 km de la entrada al parque temático SeaWorld y alberga 2 sitios para comer, una bañera de hidromasaje y una piscina al aire libre inspirada en el espectáculo Shamu. Las habitaciones disponen de baño con tocador de estilo spa. Además, hay WiFi gratuita.',9.2,'https://www.marriott.com/hotels/travel/mcoss-springhill-suites-orlando-at-seaworld/',1);

insert into hotel(ID_Hotel,Nombre,Descripcion,Puntuacion,Webpage,ID_Ciudad)
values (6,'Hôtel La Nouvelle République','El Hôtel La Nouvelle République se encuentra en el distrito 11 de París, a 15 minutos a pie de la Place de la République y a 20 minutos a pie del distrito de Marais.',9.1,'https://www.hotel-la-nouvelle-republique.paris/en/',2);

insert into hotel(ID_Hotel,Nombre,Descripcion,Puntuacion,Webpage,ID_Ciudad)
values (7,'Motel One Paris-Porte Dorée','El Motel One Paris-Porte Dorée está situado en el distrito 12 de París y alberga jardín y bar. El establecimiento se encuentra a 6 minutos a pie de la parada de metro de Porte Dorée, a 4 km de la Ópera de la Bastilla y a 6 km de la catedral de Notre Dame.',9.0,'https://www.motel-one.com/en/hotels/france/paris/hotel-paris-porte-doree/',2);

insert into hotel(ID_Hotel,Nombre,Descripcion,Puntuacion,Webpage,ID_Ciudad)
values (8,'Hyatt Centric Gran Via Madrid','El Hyatt Centric Gran Vía Madrid se encuentra en el corazón de Madrid y cuenta con un gimnasio abierto las 24 horas, un bar y un restaurante. La estación de metro de Gran Vía está a 150 metros. Además, hay WiFi gratuita en todas las instalaciones.',9.0,'https://www.hyatt.com/en-US/hotel/spain/hyatt-centric-gran-via-madrid/madct',6);

insert into hotel(ID_Hotel,Nombre,Descripcion,Puntuacion,Webpage,ID_Ciudad)
values (9,'VP Plaza España Design','El VP Plaza España Design se encuentra en la Gran Via, en el centro de Madrid, y ofrece alojamiento de 5 estrellas con aparcamiento, centro de bienestar y piscina de temporada.',9.4,'https://www.plazaespana-hotel.com/en/',6);

insert into hotel(ID_Hotel,Nombre,Descripcion,Puntuacion,Webpage,ID_Ciudad)
values (10,'Motel One Berlin-Alexanderplatz','El Motel One Berlin-Alexanderplatz ofrece habitaciones con aire acondicionado y TV vía satélite de pantalla plana y se encuentra en el distrito Mitte de Berlín.',9.0,'https://www.motel-one.com/en/hotels/berlin/hotel-berlin-alexanderplatz/',3);

insert into hotel(ID_Hotel,Nombre,Descripcion,Puntuacion,Webpage,ID_Ciudad)
values (11,'GHL Hotel Capital','El GHL Hotel Capital está situado en la avenida El Dorado de Bogotá, cerca de la embajada de los Estados Unidos y a 6 km del aeropuerto internacional El Dorado. Ofrece 2 restaurantes y un bar.',9.1,'https://en.hotelcapital.com.co/',8);

insert into hotel(ID_Hotel,Nombre,Descripcion,Puntuacion,Webpage,ID_Ciudad)
values (12,'Grand Hyatt Bogota ','El Grand Hyatt Bogota se encuentra en la localidad de Teusaquillo de Bogotá y cuenta con centro de fitness. bar y restaurante de comida local. El establecimiento se halla a 3,3 km del centro internacional de exposiciones de Corferias.',9.5,'https://www.hyatt.com/en-US/hotel/colombia/grand-hyatt-bogota/boggh',8);


Paradas
insert into viajeciudad(id_viaje,id_ciudad,id_hotel)
    values (1,2,7);

insert into viajeciudad(id_viaje,id_ciudad,id_hotel)
    values (2,4,2);

insert into viajeciudad(id_viaje,id_ciudad,id_hotel)
    values (3,1,4);

insert into viajeciudad(id_viaje,id_ciudad,id_hotel)
    values (4,7,3);

insert into viajeciudad(id_viaje,id_ciudad,id_hotel)
    values (5,6,8);
insert into viajeciudad(id_viaje,id_ciudad,id_hotel)
    values (5,3,10);

*/
=======


--Entradas del blog
create table Blog(
    id int primary key,
    titulo varchar(255),
    autor varchar(255),
    contenido text
);

DELIMITER $$
create procedure getAllBlogs()
BEGIN
    select id,titulo,autor,contenido from Blog;
END$$

DELIMITER $$
create procedure getAllBlogsShort()
BEGIN
    select id,titulo,autor,SUBSTRING(contenido,1,50) as contenido from Blog;
END$$

DELIMITER $$
create procedure getBlogPorId(nid int)
BEGIN
    select id,titulo,autor,contenido from Blog where id=nid;
END$$

DELIMITER ;

insert into Blog(id,titulo, autor, contenido) 
        values (1,"Cancun: una experiencia maravillosa","Federico de Palermo",
        "Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas Letraset, las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum."
        );

insert into Blog(id,titulo, autor, contenido) 
        values (2,"Disneylandia, para volver a ser niños","El Marques de carabas",
        "Al contrario del pensamiento popular, el texto de Lorem Ipsum no es simplemente texto aleatorio. Tiene sus raices en una pieza cl´sica de la literatura del Latin, que data del año 45 antes de Cristo, haciendo que este adquiera mas de 2000 años de antiguedad. Richard McClintock, un profesor de Latin de la Universidad de Hampden-Sydney en Virginia, encontró una de las palabras más oscuras de la lengua del latín, consecteur, en un pasaje de Lorem Ipsum, y al seguir leyendo distintos textos del latín, descubrió la fuente indudable. Lorem Ipsum viene de las secciones 1.10.32 y 1.10.33 de de Finnibus Bonorum et Malorum (Los Extremos del Bien y El Mal) por Cicero, escrito en el año 45 antes de Cristo. Este libro es un tratado de teoría de éticas, muy popular durante el Renacimiento. La primera linea del Lorem Ipsum, Lorem ipsum dolor sit amet.., viene de una linea en la sección 1.10.32

        El trozo de texto estándar de Lorem Ipsum usado desde el año 1500 es reproducido debajo para aquellos interesados. Las secciones 1.10.32 y 1.10.33 de de Finibus Bonorum et Malorum por Cicero son también reproducidas en su forma original exacta, acompañadas por versiones en Inglés de la traducción realizada en 1914 por H. Rackham."
        );

insert into Blog(id,titulo, autor, contenido) 
        values (3,"Viajes fabulosos a Suesca","Chayanne",
        "Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo Contenido aquí, contenido aquí. Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de Lorem Ipsum va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo)."
        );

insert into Blog(id,titulo, autor, contenido) 
        values (4,"Como sacarle el maximo provecho a tus millas de viaje","Donald Trump",
        "Hay muchas variaciones de los pasajes de Lorem Ipsum disponibles, pero la mayoría sufrió alteraciones en alguna manera, ya sea porque se le agregó humor, o palabras aleatorias que no parecen ni un poco creíbles. Si vas a utilizar un pasaje de Lorem Ipsum, necesitás estar seguro de que no hay nada avergonzante escondido en el medio del texto. Todos los generadores de Lorem Ipsum que se encuentran en Internet tienden a repetir trozos predefinidos cuando sea necesario, haciendo a este el único generador verdadero (válido) en la Internet. Usa un diccionario de mas de 200 palabras provenientes del latín, combinadas con estructuras muy útiles de sentencias, para generar texto de Lorem Ipsum que parezca razonable. Este Lorem Ipsum generado siempre estará libre de repeticiones, humor agregado o palabras no características del lenguaje, etc.."
        );
>>>>>>> 56cc116615719a82ff2167684d1b7b9e441cb674


alter table viaje drop column fecha_inicio;
alter table viaje drop column fecha_fin;
alter table viaje add column dias int;
update viaje set dias =5 where id_viaje >3;
update viaje set dias =8 where id_viaje =3;
update viaje set dias =4 where id_viaje <3;
drop procedure getallviajes;
DELIMITER $$
CREATE procedure getAllViajes()
BEGIN
select id_viaje, nombre, cupos_restantes, precio_menor, precio_adulto, dias, id_aerolinea, descripcion, thumbnail from Viaje;
END$$



drop procedure buscarViajePorCiudad;
DELIMITER $$
create procedure buscarViajePorCiudad(nID_Ciudad int)
BEGIN
    select V.id_viaje, nombre, descripcion, thumbnail, 
    cupos_restantes, precio_menor, precio_adulto,
    id_aerolinea, dias from Viaje as V, ViajeCiudad as VC
    where V.id_viaje = VC.id_viaje and VC.id_ciudad = nid_ciudad;
END$$

DELIMITER $$
create procedure getAllCiudades()
BEGIN
    select id_ciudad, nombre, thumbnail, informacion from Ciudad;
END$$
drop procedure getViajeporId;
DELIMITER $$
CREATE procedure getViajeporId(nid int)
BEGIN
select id_viaje, nombre, cupos_restantes, precio_menor, precio_adulto, dias, id_aerolinea, descripcion, thumbnail from Viaje where id_viaje = nid;
END$$

DELIMITER ;

alter table Blog add column (thumbnail varchar(500));

drop procedure getBlogPorId;
DELIMITER $$
CREATE procedure getBlogporId(nid int)
BEGIN
select id,titulo,autor,contenido,thumbnail from Blog where id=nid;
END$$

DELIMITER ;
update blog set thumbnail="https://www.lavanguardia.com/r/GODO/LV/p5/WebSite/2018/06/15/Recortada/img_lbernaus_20180615-100456_imagenes_lv_terceros_istock-492416114-109-kn3E-U45119239404E0-992x558@LaVanguardia-Web.jpg" where id=1;
update blog set thumbnail="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYyABKkyeTVcPoZS2tczrYatrV4HSCdmTsDJ7YmKR2-oEDXC0q" where id=2;
update blog set thumbnail="https://s04.s3c.es/imag/_v0/4961x3311/d/d/f/700x420_viajes.jpg" where id=3;
update blog set thumbnail="http://www.surferrule.com/surferrule/wp-content/uploads/2017/07/Hora-de-viajar.jpg" where id=4;

drop procedure getAllBlogs;
DELIMITER $$

create procedure getAllBlogs()
BEGIN
    select id,titulo,autor,contenido,thumbnail from Blog;
END$$

drop procedure getAllBlogsShort$$

create procedure getAllBlogsShort()
BEGIN
    select id,titulo,autor,SUBSTRING(contenido,1,50) as contenido, thumbnail from Blog;
END$$

DELIMITER ;

DELIMITER $$
create procedure buscarCiudadPorViaje(nID_Viaje int)
BEGIN
    select C.id_ciudad, nombre, thumbnail, informacion from Ciudad as C, ViajeCiudad as VC
    where VC.id_viaje = nID_Viaje and C.id_ciudad = VC.id_ciudad;
END$$

DELIMITER $$
create procedure postNewBlog(ntitulo varchar(255), nautor varchar(255), ncontenido text, nthumbnail varchar(500))
BEGIN
    insert into blog(titulo,autor,contenido,thumbnail) values (ntitulo,nautor,ncontenido,nthumbnail);
END$$

alter table Blog modify column id int auto_increment$$